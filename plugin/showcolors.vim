function! GetSyntaxID()
    return synID(line('.'), col('.'), 1)
endfunction

function! GetSyntaxParentID()
    return synIDtrans(GetSyntaxID())
endfunction

function! GetSyntax()
    if exists("*synstack")
        let syn_lst = map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
        let syn_rez = ""
        for i in syn_lst
            let syn_rez .= i . " > "
        endfor
        echo syn_rez[0:-3]
    else
        echo synIDattr(GetSyntaxID(), 'name')
    endif
    exec "hi ".synIDattr(GetSyntaxParentID(), 'name')
endfunction

command! -nargs=0 GetItemDescription call GetSyntax()
