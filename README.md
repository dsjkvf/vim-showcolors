# Showcolors: debugging the colorscheme

## About

This is a Vim plugin for testing and editing Vim colorschemes by providing detailed syntax related information for the element selected. All the credits [go](https://www.reddit.com/r/vim/comments/3duumy/changing_markdown_syntax_colors/ct90v0z) to the reddit user [magus424](https://www.reddit.com/user/magus424).

## Configuration

Since the only available command is `GetItemDescription`, one might be interested in adding something like the following to their `.vimrc`:

    nnoremap <Leader>z  :GetItemDescription<CR>
